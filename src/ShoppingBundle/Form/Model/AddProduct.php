<?php

namespace ShoppingBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

use ShoppingBundle\Entity\Product;
use ShoppingBundle\Entity\Type;

class AddProduct
    {
    /**
     * @Assert\Type(type="ShoppingBundle\Entity\Product")
     * @Assert\Valid()
     */
    protected $product;

    public function setProduct(Product $product)
    {
        $this->product = $product;
    }

    public function getProduct()
    {
        return $this->product;
    }

    }

?>