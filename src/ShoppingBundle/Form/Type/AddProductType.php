<?php

namespace ShoppingBundle\Form\Type;

use ShoppingBundle\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AddProductType extends AbstractType
{
public function buildForm(FormBuilderInterface $builder, array $options)
{
$builder->add('product', new ProductType());

$builder->add('Add-Product', 'submit');
}

public function getName()
{
return 'Product';
}
}

?>