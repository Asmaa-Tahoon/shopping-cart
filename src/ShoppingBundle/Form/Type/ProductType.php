<?php
namespace ShoppingBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use ShoppingBundle\Entity\Type;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('name', 'text');
        $builder->add('price', 'integer');
        $builder->add('description', 'textarea');
        $builder->add('type', 'entity', array(
            'class' => 'ShoppingBundle:Type',
            'choice_label' => 'name',
            'placeholder' => 'Choose type',
        ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ShoppingBundle\Entity\Product'
        ));
    }

    public function getName()
    {
        return 'product';
    }
}

?>