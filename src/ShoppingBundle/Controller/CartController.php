<?php

namespace ShoppingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ShoppingBundle\Entity\Cart;
use Symfony\Component\HttpFoundation\Response;


abstract class CartController extends Controller
{
//	public function createAction()
//    {
//
//	$cart = new Cart();
//    	$cart->setName('wish-list');
//
//    	$em = $this->getDoctrine()->getManager();
//
//    	$em->persist($cart);
//    	$em->flush();
//
//    	return new Response('Created cart id '.$cart->getId());
//    }

	abstract public function showProductsAction();

	abstract public function deleteFromCartAction($id);
}
