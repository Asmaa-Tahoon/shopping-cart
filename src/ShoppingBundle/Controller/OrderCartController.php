<?php

namespace ShoppingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ShoppingBundle\Entity\Cart;
use ShoppingBundle\Entity\OrderCart;
use ShoppingBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Response;

class OrderCartController extends CartController
{

    public function showProductsAction()
    {
        $products = $this->getDoctrine()
            ->getRepository('ShoppingBundle:Cart')
            ->findOrderProducts();

        if (!$products) {}

        return $this->render('ShoppingBundle:Cart:order.html.twig', array(
            'products'      => $products,

        ));
    }

    public function deleteFromCartAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('ShoppingBundle:Product')->find($id);

        $product->setCart(null);
        $em->flush();

        //return $this->redirect($this->get('referer'));

        return $this->redirectToRoute('shopping_showOrder');
    }

    public function emptyCartAction()
    {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository('ShoppingBundle:Cart')->findOrderProducts();

        if (!$products) {}

        foreach($products as $one) {
            $one->setCart(null);

        }

        $em->flush();
        return $this->redirectToRoute('shopping_showOrder');



    }

}
