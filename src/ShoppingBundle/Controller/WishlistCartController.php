<?php

namespace ShoppingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ShoppingBundle\Entity\Cart;
use ShoppingBundle\Entity\WishlistCart;
use Symfony\Component\HttpFoundation\Response;

class WishlistCartController extends CartController
{
    public function createAction()
    {

        $cart = new Cart();
        $cart->setName('wish-list');

        $em = $this->getDoctrine()->getManager();

        $em->persist($cart);
        $em->flush();

        return new Response('Created cart id '.$cart->getId());
    }

    public function showProductsAction()
    {
        $products = $this->getDoctrine()
            ->getRepository('ShoppingBundle:Cart')
            ->findWishListProducts();

        if (!$products) {}

        return $this->render('ShoppingBundle:Cart:wishList.html.twig', array(
            'products'      => $products,

        ));
    }

    public function deleteFromCartAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('ShoppingBundle:Product')->find($id);

        $product->setCart(null);
        $em->flush();

        return $this->redirectToRoute('shopping_wishList');
    }
}
