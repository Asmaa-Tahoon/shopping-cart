<?php

namespace ShoppingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use ShoppingBundle\Entity\Product;
use ShoppingBundle\Entity\Order;
use ShoppingBundle\Entity\Cart;
use ShoppingBundle\Entity\Order_products;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{
    public function checkOutAction(Request $request)
    {

        $order = new Order();
        $date = new \DateTime("now");

        $order->setDate($date);
        $em = $this->getDoctrine()->getManager();

        $em->persist($order);
        $em->flush();

        $products = $this->getDoctrine()
            ->getRepository('ShoppingBundle:Cart')
            ->findOrderProducts();

        if (!$products) {
        } else {

            /** @var Product $one */
            foreach ($products as $one) {
                $order_products = new Order_products();
                $order_products->setOrder($order);
                $order_products->setProduct($one);


                if ($request->get('submit')) {
                    $quantity = $request->get($one->getId());
                    dump($quantity);
                    $order_products->setQty($quantity);
                } else {
                    $quantity = 'Not submitted yet';
                }

                $em = $this->getDoctrine()->getManager();

                $em->persist($order_products);
                $em->flush();

            }
        }

//        return new Response('Created order id '.$order->getId());

        $products = $this->getDoctrine()
            ->getRepository('ShoppingBundle:Order')
            ->findCheckoutOrder($order->getId());

        if (!$products) {}



        $em = $this->getDoctrine()->getManager();
        $products2 = $em->getRepository('ShoppingBundle:Cart')->findOrderProducts();

        if (!$products2) {}

        foreach($products2 as $one) {
            $one->setCart(null);

        }

        $em->flush();

        return  $this->render('ShoppingBundle:Order:checkout.html.twig', array(
            'products' => $products,
        ));
    }


}
