<?php

namespace ShoppingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ShoppingBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{

    public function uploadAction(Request $request)
    {
        $user=new User();
        $form= $this->createFormBuilder($user)
            ->add('name')
            ->add('file')
            ->add('file2')
            ->add('add','submit')
            ->getForm();

        $form->handleRequest($request);

        if($form->isValid())
        {
            $em=$this->getDoctrine()->getManager();

          //  $user->upload();

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('shopping_list');
        }

        return $this->render('ShoppingBundle:User:adduser.html.twig', array('form' => $form->createView()));
      //  return array('form' => $form->createView());
    }

    public function showUserAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository('ShoppingBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find user');
        }



        if($user->getPath()==null)
        {

            $form= $this->createFormBuilder($user)
                ->add('file')
                ->add('file2')
                ->add('upload-Image','submit')
                ->getForm();

            $form->handleRequest($request);

        if($form->isValid())
        {
            $em=$this->getDoctrine()->getManager();

            //  $user->upload();
            $em->flush();

            return $this->redirectToRoute('listusers');
        }

           return $this->render('ShoppingBundle:User:show.html.twig', array('user' => $user,'form' => $form->createView()));
        }
        else {

            return $this->render('ShoppingBundle:User:show.html.twig', array(
                'user' => $user
            ));
        }
    }

    public function listUsersAction()
    {
        $em=$this->getDoctrine()->getManager();
        $users = $em->getRepository('ShoppingBundle:User')->findAll();

        if (!$users) {
            throw $this->createNotFoundException('Unable to find users');
        }

        return $this->render('ShoppingBundle:User:listusers.html.twig', array(
            'users'      => $users
        ));

    }

    public function removeImageAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository('ShoppingBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find user');
        }

        $user->removeUpload();
        $user->removeUpload2();

        $user->setPath(null);
        $user->setPath2(null);
        $em->flush();

        return $this->redirectToRoute('listusers');

    }

}
