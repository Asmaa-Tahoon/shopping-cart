<?php

namespace ShoppingBundle\Controller;


use ShoppingBundle\Form\Type\AddProductType;
use ShoppingBundle\Form\Type\UpdateProductType;
use ShoppingBundle\Form\Type\ProductType;
use ShoppingBundle\Form\Model\AddProduct;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ShoppingBundle\Entity\Product;
use ShoppingBundle\Entity\Type;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ShoppingBundle:Default:index.html.twig', array('name' => $name));
    }

    public function createAction()
    {
	$type = new Type();
	$type->setName('normal');

	$product = new Product();
    	$product->setName('Second Product');
    	$product->setPrice('40');
    	$product->setDescription('its the third with sale');
	$product->setType($type);

    	$em = $this->getDoctrine()->getManager();

    	$em->persist($product);
	$em->persist($type);
    	$em->flush();

    	return new Response('Created product id '.$product->getId()
				.'  and Type id: '.$type->getId());
    }


    public function listAction()
    {
    $products = $this->getDoctrine()
        ->getRepository('ShoppingBundle:Product')
        ->findAllProductsWithTypes();

    if (!$products) {
        throw $this->createNotFoundException(
            'No products found'
        );}

	return $this->render('ShoppingBundle:Product:list.html.twig', array(
            'products'      => $products,

        ));

	
    }


    public function ProductToOrderAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('ShoppingBundle:Product')->find($id);

        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        $cart = $em->getReference('ShoppingBundle\Entity\Cart', 2);
        $product->setCart($cart);

        $em->flush();

        return $this->redirectToRoute('shopping_list');
    }

    public function ProductToWishListAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('ShoppingBundle:Product')->find($id);

        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        $cart = $em->getReference('ShoppingBundle\Entity\Cart', 3);
        $product->setCart($cart);

        // $product->setCart_id(2);
        $em->flush();

        return $this->redirectToRoute('shopping_list');
    }

    public function addAction()
    {
        $product = new AddProduct();
        $form = $this->createForm(new AddProductType(), $product, array(
            'action' => $this->generateUrl('product_create'),
        ));

        return $this->render(
            'ShoppingBundle:Product:add.html.twig',
            array('form' => $form->createView())
        );
    }

    public function submitAddAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new AddProductType(), new AddProduct());

        $form->handleRequest($request);

        if ($form->isValid()) {
            $product = $form->getData();

            $em->persist($product->getProduct());
            $em->flush();

            return $this->redirectToRoute('shopping_list');
        }

        return $this->render(
            'ShoppingBundle:Product:add.html.twig',
            array('form' => $form->createView())
        );

    }

    public function updateAction($id,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('ShoppingBundle:Product')->find($id);

        $form = $this->createForm(new ProductType(), $product);
        //$form = $this->createForm(new UpdateProductType(), $product);


        if ($form->isValid()) {
                $editPlan = $form->getData();
                $em->flush();
            }

        return $this->render('ShoppingBundle:Product:update.html.twig', array('form' => $form->createView()));

    }

//    public function newAction()
//    {
//        $product=$this->getDoctrine()->getManager();
//        $form=$this->createForm("product",$product);
//
//        return $this->render(
//            'ShoppingBundle:Product:new.html.twig',
//            array('form' => $form->createView())
//        );
//
//    }

}