<?php
// src/Blogger/BlogBundle/DataFixtures/ORM/BlogFixtures.php

namespace ShoppingBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ShoppingBundle\Entity\Type;

class TypeFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $type1 = new Type();
	$type1->setId(1);
        $type1->setName('Sale 50%');
        $manager->persist($type1);

        $type2 = new Type();
	$type2->setId(2);
	$type2->setName('normal');
        $manager->persist($type2);

	$manager->flush();

        $this->addReference('type-1', $type1);
        $this->addReference('type-2', $type2);
    }

    public function getOrder()
    {
        return 1;
    }

}
